package com.develcode.userRegistration.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.develcode.userRegistration.domain.Photo;

/**
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * TODO Definir documentação da classe. <br>
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 16 de abr de 2021 - @author Kennedy Assunção - Primeira versão da classe.
 * <br>
 * <br>
 * <br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

public interface PhotoRepository extends JpaRepository < Photo , Long > {

}
