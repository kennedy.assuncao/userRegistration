package com.develcode.userRegistration.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * TODO Definir documentação da classe. <br>
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 16 de abr de 2021 - @author Kennedy Assunção - Primeira versão da classe.
 * <br>
 * <br>
 * <br>
 * LISTA DE CLASSES INTERNAS: <br>
 */
@Entity
@Table ( name = "foto" )
public class Photo {
    @Id
    @GeneratedValue ( strategy = GenerationType.IDENTITY )
    private Long codigo;
    private String nome;
    private String link;
    @OneToOne
    @JoinColumn ( name = "codigo_id" )
    private User usuario;

    /**
     * Método de recuperação do campo codigo
     *
     * @return valor do campo codigo
     */
    public Long getCodigo () {
        return codigo;
    }

    /**
     * Valor de codigo atribuído a codigo
     *
     * @param codigo
     *            Atributo da Classe
     */
    public void setCodigo ( Long codigo ) {
        this.codigo = codigo;
    }

    /**
     * Método de recuperação do campo nome
     *
     * @return valor do campo nome
     */
    public String getNome () {
        return nome;
    }

    /**
     * Valor de nome atribuído a nome
     *
     * @param nome
     *            Atributo da Classe
     */
    public void setNome ( String nome ) {
        this.nome = nome;
    }

    /**
     * Método de recuperação do campo link
     *
     * @return valor do campo link
     */
    public String getLink () {
        return link;
    }

    /**
     * Valor de link atribuído a link
     *
     * @param link
     *            Atributo da Classe
     */
    public void setLink ( String link ) {
        this.link = link;
    }

    /**
     * Método de recuperação do campo usuario
     *
     * @return valor do campo usuario
     */
    public User getUsuario () {
        return usuario;
    }

    /**
     * Valor de usuario atribuído a usuario
     *
     * @param usuario
     *            Atributo da Classe
     */
    public void setUsuario ( User usuario ) {
        this.usuario = usuario;
    }

    /**
     * TODO Descrição do Método
     * 
     * @return
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString () {
        return "Photo [codigo=" + codigo + ", nome=" + nome + ", link=" + link + ", usuario="
                + usuario + "]";
    }

    /**
     * TODO Descrição do Método
     * 
     * @return
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode () {
        final int prime = 31;
        int result = 1;
        result = prime * result + ( ( codigo == null ) ? 0 : codigo.hashCode() );
        return result;
    }

    /**
     * TODO Descrição do Método
     * 
     * @param obj
     * @return
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals ( Object obj ) {
        if ( this == obj )
            return true;
        if ( obj == null )
            return false;
        if ( getClass() != obj.getClass() )
            return false;
        Photo other = ( Photo ) obj;
        if ( codigo == null ) {
            if ( other.codigo != null )
                return false;
        } else if ( ! codigo.equals( other.codigo ) )
            return false;
        return true;
    }

}
