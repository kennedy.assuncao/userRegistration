package com.develcode.userRegistration.domain;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * TODO Definir documentação da classe. <br>
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 16 de abr de 2021 - @author Kennedy Assunção - Primeira versão da classe.
 * <br>
 * <br>
 * <br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

@Entity
@Table ( name = "usuario" )
public class User {

    @Id
    @GeneratedValue ( strategy = GenerationType.IDENTITY )
    private Long codigo;
    private String nome;
    private LocalDate dataNascimento;
    @OneToOne
    @JoinColumn ( name = "codigo_id" )
    private Photo foto;

    /**
     * Método de recuperação do campo codigo
     *
     * @return valor do campo codigo
     */
    public Long getCodigo () {
        return codigo;
    }

    /**
     * Valor de codigo atribuído a codigo
     *
     * @param codigo
     *            Atributo da Classe
     */
    public void setCodigo ( Long codigo ) {
        this.codigo = codigo;
    }

    /**
     * Método de recuperação do campo nome
     *
     * @return valor do campo nome
     */
    public String getNome () {
        return nome;
    }

    /**
     * Valor de nome atribuído a nome
     *
     * @param nome
     *            Atributo da Classe
     */
    public void setNome ( String nome ) {
        this.nome = nome;
    }

    /**
     * Método de recuperação do campo dataNascimento
     *
     * @return valor do campo dataNascimento
     */
    public LocalDate getDataNascimento () {
        return dataNascimento;
    }

    /**
     * Valor de dataNascimento atribuído a dataNascimento
     *
     * @param dataNascimento
     *            Atributo da Classe
     */
    public void setDataNascimento ( LocalDate dataNascimento ) {
        this.dataNascimento = dataNascimento;
    }

    /**
     * Método de recuperação do campo foto
     *
     * @return valor do campo foto
     */
    public Photo getFoto () {
        return foto;
    }

    /**
     * Valor de foto atribuído a foto
     *
     * @param foto
     *            Atributo da Classe
     */
    public void setFoto ( Photo foto ) {
        this.foto = foto;
    }

    /**
     * TODO Descrição do Método
     * 
     * @return
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString () {
        return "User [codigo=" + codigo + ", nome=" + nome + ", dataNascimento=" + dataNascimento
                + ", foto=" + foto + "]";
    }

    /**
     * TODO Descrição do Método
     * 
     * @return
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode () {
        final int prime = 31;
        int result = 1;
        result = prime * result + ( ( codigo == null ) ? 0 : codigo.hashCode() );
        return result;
    }

    /**
     * TODO Descrição do Método
     * 
     * @param obj
     * @return
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals ( Object obj ) {
        if ( this == obj )
            return true;
        if ( obj == null )
            return false;
        if ( getClass() != obj.getClass() )
            return false;
        User other = ( User ) obj;
        if ( codigo == null ) {
            if ( other.codigo != null )
                return false;
        } else if ( ! codigo.equals( other.codigo ) )
            return false;
        return true;
    }

}
