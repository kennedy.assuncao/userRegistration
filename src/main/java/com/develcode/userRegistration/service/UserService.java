package com.develcode.userRegistration.service;

import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.develcode.userRegistration.domain.User;
import com.develcode.userRegistration.repository.UserRepository;
import com.develcode.userRegistration.resource.UserResource;

/** 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * TODO Definir documentação da classe. <br>
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 16 de abr de 2021 - @author Kennedy Assunção - Primeira versão da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

@Service
@Transactional
public class UserService {
    
    private final Logger log = LoggerFactory.getLogger(UserService.class);
    
    @Autowired
    UserRepository repository;

    /** 
     * TODO Descrição do Método
     * @param user
     * @return
     */
    public User save ( User user ) {
        log.debug( "Request to save Users : {}" , user );
        return repository.save( user );
    }

    /** 
     * TODO Descrição do Método
     * @return
     */
    public List < User > findAll () {
        log.debug("Request to find all Users");
        return repository.findAll();
    }

}
