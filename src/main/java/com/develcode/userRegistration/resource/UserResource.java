package com.develcode.userRegistration.resource;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.develcode.userRegistration.domain.User;
import com.develcode.userRegistration.exception.BadRequestAlertException;
import com.develcode.userRegistration.service.UserService;

/**
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * TODO Definir documentação da classe. <br>
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 16 de abr de 2021 - @author Kennedy Assunção - Primeira versão da classe.
 * <br>
 * <br>
 * <br>
 * LISTA DE CLASSES INTERNAS: <br>
 */
@RestController
@RequestMapping ( "/api" )
public class UserResource {
    
    private final Logger log = LoggerFactory.getLogger(UserResource.class);
    private static final String ENTITY_NAME = "Usuario";
    
    @Autowired
    UserService userService;
    
    @PostMapping("/usuario")
    public ResponseEntity < User > createUser (@RequestBody User user ) throws URISyntaxException{
        log.debug( "REST request to save User : {}" , user );
        if ( user.getCodigo() != null ) {
            throw new BadRequestAlertException( "New User cannot already hava an ID" , ENTITY_NAME ,
                    "idExists" );
        }
        User resul = userService.save( user );
        return ResponseEntity.created( new URI( "/api/usuario" + resul.getCodigo() ) ).build();
    }
    
    @GetMapping("/usuario")
    public List < User > findAll(){
        log.debug("REST request to find all Users");
        return userService.findAll();
    }

}
