package com.develcode.userRegistration.exception;

import java.net.URI;

/** 
 * DOCUMENTAÇÃO DA CLASSE <br>
 * ---------------------- <br>
 * FINALIDADE: <br>
 * TODO Definir documentação da classe. <br>
 * <br>
 * HISTÓRICO DE DESENVOLVIMENTO: <br>
 * 16 de abr de 2021 - @author Kennedy Assunção - Primeira versão da classe. <br>
 *<br>
 *<br>
 * LISTA DE CLASSES INTERNAS: <br>
 */

public final class ErrorConstants {

    public static final URI DEFAULT_TYPE = URI.create("/problem-with-user");
}
